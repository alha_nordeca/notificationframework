package com.nordeca.notificationframework;

public class Count {
	
	int count;
	
	public Count(int i) {
		count = i;
	}
	
	public int getCount() {
		return count; 
	}
	
	public void add(int i) {
		count += i;
	}

}
