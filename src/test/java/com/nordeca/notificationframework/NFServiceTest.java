package com.nordeca.notificationframework;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.activemq.broker.BrokerService;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.nordeca.notificationframework.exception.NFException;

public class NFServiceTest {
	
	private static final String embeddedBrokerUrl = "tcp://localhost:61617";
	private static BrokerService broker;
	private static NFService nfservice;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		broker = new BrokerService();
		broker.addConnector(embeddedBrokerUrl);
		broker.setDataDirectory("/tmp/activemq-data");
		broker.start();
		broker.waitUntilStarted();
		nfservice = NFService.getInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		broker.stop();
		broker.waitUntilStopped();
		File temp = broker.getDataDirectoryFile();
		FileUtils.deleteDirectory(temp);
		broker = null;
		nfservice = null;
	}

	@Before
	public void setUp() throws Exception {
		nfservice.setBrokerURL(embeddedBrokerUrl);
		nfservice.setBrokerUserName("");
		nfservice.setBrokerPassword("");
		nfservice.setEventBusName("event.bus");
		nfservice.connect();
	}

	@After
	public void tearDown() throws Exception {
		broker.deleteAllMessages();
		nfservice.reset();
	}

//	@Test
//	public void testBroadcast1() {
//		System.out.println("test1");
//		NFEvent dummyEvent = new NFEvent("add");
//		dummyEvent.putDataItem("add", 1);
//		
//		final Count c = new Count(0);
//		NFEventListener listener = new NFEventListener() {
//			public void onEvent(NFEvent event) {
//				System.out.println("recieved");
//				int term = (Integer) event.getDataItem("add");
//				c.add(term);
//			}
//		};
//		System.out.println("ayy");
//		nfservice.registerListenerToEventBus("add", listener);
//		assertEquals(0, c.getCount());
//		
//		try {
//			nfservice.broadcastEvent(dummyEvent);
//			// Wait 2 sec for onEvent.
//			Thread.sleep(2000);
//			assertEquals(1, c.getCount());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	@Test
	public void testBroadcast2() throws NFException, InterruptedException {
		System.out.println("test2");
		NFEvent dummyEvent = new NFEvent("add");
		dummyEvent.putDataItem("add", 1);
		
		final Count c = new Count(0);
		NFEventListener listener = new NFEventListener() {
			public void onEvent(NFEvent event) {
				int term = (Integer) event.getDataItem("subtract");
				c.add(-term);
			}
		};
		
		nfservice.registerListenerToEventBus("subtract", listener);
		assertEquals(0, c.getCount());
		
		nfservice.broadcastEvent(dummyEvent);
		// Wait 2 sec for onEvent.
		Thread.sleep(2000);
		assertEquals(0, c.getCount());
	}
//	
//	@Test
//	public void testQueue1() throws JMSException, InterruptedException {
//		NFEvent dummyEvent = new NFEvent("add");
//		dummyEvent.putDataItem("add", 1);
//		
//		final Count c = new Count(0);
//		NFEventListener listener = new NFEventListener() {
//			@Override
//			public void onEvent(NFEvent event) {
//				int term = (Integer) event.getDataItem("add");
//				c.add(term);
//			}
//		};
//		
//		nfservice.registerListenerToQueue("test1", listener);
//		assertEquals(0, c.getCount());
//		
//		nfservice.sendEvent("test2", dummyEvent);
//		Thread.sleep(2000);
//		assertEquals(0, c.getCount());
//		
//		nfservice.sendEvent("test1", dummyEvent);
//		Thread.sleep(2000);
//		assertEquals(1, c.getCount());
//	}
	
}
