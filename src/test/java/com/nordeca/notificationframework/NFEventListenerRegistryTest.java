package com.nordeca.notificationframework;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

public class NFEventListenerRegistryTest {
	
	private NFEventListenerRegistry listenerRegistry;
	
	@Before
	public void setUp() {
		listenerRegistry = new NFEventListenerRegistry();
	}

	@Test
	public void testRegisterListenerToEventBus() {
		String eventType = "dummy";
		NFEventListener dummyListener = new NFEventListener() {
			public void onEvent(NFEvent event) {
				// Doesn't have to do anything.
			}
		};
		
		boolean didRegister = listenerRegistry.registerListenerToEventBus(eventType, dummyListener);
		assertEquals(true, didRegister);
		
		List<NFEventListener> registeredListeners = listenerRegistry.getEventBusListeners(eventType);
		assertEquals(1, registeredListeners.size());
	}
	
	@Test
	public void testDeregisterListenerFromEventBus() {
		String eventType = "dummy";
		NFEventListener dummyListener = new NFEventListener() {
			public void onEvent(NFEvent event) {
				// Doesn't have to do anything.
			}
		};
		
		listenerRegistry.registerListenerToEventBus(eventType, dummyListener);
		List<NFEventListener> registeredListeners = listenerRegistry.getEventBusListeners(eventType);
		assertEquals(1, registeredListeners.size());
		boolean didDeregister = listenerRegistry.deregisterListenerFromEventBus(eventType, dummyListener);
		assertEquals(true, didDeregister);
		assertEquals(0, registeredListeners.size());
	}
	
	@Test
	public void testRegisterListenerToQueue() {
		String queueName = "testqueue";
		DefaultMessageListenerContainer listenerContainer = new DefaultMessageListenerContainer();
		
		boolean didRegister = listenerRegistry.registerListenerToQueue(queueName, listenerContainer);
		assertEquals(true, didRegister);
		
		assertEquals(listenerContainer, listenerRegistry.getQueueListeningContainers(queueName));
		
		listenerContainer.destroy();
	}
	
	@Test
	public void testDeregisterListenerFromQueue() {
		String queueName = "testqueue";
		DefaultMessageListenerContainer listenerContainer = new DefaultMessageListenerContainer();
		
		boolean didRegister = listenerRegistry.registerListenerToQueue(queueName, listenerContainer);
		assertEquals(true, didRegister);
		
		boolean didDeregister = listenerRegistry.deregisterListenerFromQueue(queueName);
		assertEquals(true, didDeregister);
		
		assertEquals(null, listenerRegistry.getQueueListeningContainers(queueName));
		
		listenerContainer.destroy();
	}

}
