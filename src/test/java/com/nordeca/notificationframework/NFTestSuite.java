package com.nordeca.notificationframework;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NFEventListenerRegistryTest.class, NFServiceTest.class })
public class NFTestSuite {

}
