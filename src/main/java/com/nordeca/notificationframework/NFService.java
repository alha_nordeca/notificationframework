package com.nordeca.notificationframework;

import java.util.Map;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import com.nordeca.notificationframework.exception.NFException;

/**
 * Core class for the Notification Framework. Connects to the event bus topic and distributes NFEvents to the 
 * registered NFEventListeners. Also has functions for broadcasting NFEvents to the event bus topic.
 * 
 * @author alha
 *
 */
public class NFService {
	
	private static final Logger logger = LoggerFactory.getLogger(NFService.class);
	private static NFService nf_service = new NFService();
	
	private NFEventListenerRegistry listenerRegistry;	
	private DefaultMessageListenerContainer eventBusListenerContainer;
	
	// Broker credentials. 
	private String brokerURL;
	private String brokerUsername;
	private String brokerPassword;
	
	// Event bus topic name.
	private String eventBusTopicName;
	
	private boolean connected = false;
	
	private NFService() { 
		listenerRegistry = new NFEventListenerRegistry();
	}
	
	/**
	 * Get singleton instance.
	 * 
	 * @return
	 */
	public static NFService getInstance() {
		return nf_service;
	}
	
	/**
	 * Resets the singleton instance. Clears the listener registry.
	 */
	public void reset() {
		disconnect();
		listenerRegistry = new NFEventListenerRegistry();
	}

	/**
	 * Connect to the event bus topic.
	 * @throws NFException 
	 */
	public void connect() throws NFException {
		if (connected) {
			// Already connected.
			return;
		}
		
		if (brokerURL != null && brokerUsername != null && brokerPassword != null && eventBusTopicName != null) {
			MessageListener eventBusListener = new NFBusListener(listenerRegistry);
			eventBusListenerContainer = createListenerContainer(eventBusTopicName, eventBusListener, true);
			eventBusListenerContainer.start();
			connected = true;
		} else {
			throw new NFException("NFService needs to be configured with brokerURL, brokerUsername, brokerPassword and eventBusName.");
		}
	}
	
	/**
	 * Disconnect the event bus topic.
	 */
	public void disconnect() {
		eventBusListenerContainer.stop();
		eventBusListenerContainer.destroy();
		eventBusListenerContainer = null;
		connected = false;
	}
	
	/**
	 * Is connected to the event bus topic.
	 * 
	 * @return
	 */
	public boolean isConnected() {
		return connected;
	}
	
	/**
	 * Create configured ConnectionFactory.
	 * 
	 * @return
	 */
	private ConnectionFactory createConnectionFactory() {
		PooledConnectionFactory connectionFactory = new PooledConnectionFactory();
		ActiveMQConnectionFactory activeConnFactory = new ActiveMQConnectionFactory(brokerUsername, brokerPassword, brokerURL);
		activeConnFactory.setTrustAllPackages(true);
		connectionFactory.setConnectionFactory(activeConnFactory);
		connectionFactory.setMaxConnections(10);
		
		return connectionFactory;
	}
	
	/**
	 * Create listening container.
	 * 
	 * @param connectionFactory
	 * @param destination
	 * @param listener
	 * @param broadcast
	 * @return
	 */
	private DefaultMessageListenerContainer createListenerContainer(String destination, MessageListener listener, boolean broadcast) {	
		DefaultMessageListenerContainer listenerContainer = new DefaultMessageListenerContainer();
		listenerContainer.setConnectionFactory(createConnectionFactory());
		listenerContainer.setDestinationName(destination);
		listenerContainer.setPubSubDomain(broadcast);			
		listenerContainer.setMessageListener(listener);
		listenerContainer.afterPropertiesSet();
		
		return listenerContainer;
	}
	
	/**
	 * Register NFEventListener to eventType on the event bus. You can register as many listeners as you wish to each eventType.
	 * 
	 * @param eventType
	 * @param listener
	 */
	public boolean registerListenerToEventBus(String eventType, NFEventListener listener) {
		return listenerRegistry.registerListenerToEventBus(eventType, listener);
	}
	
	/**
	 * 
	 * @param eventType
	 * @param listener
	 * @return
	 */
	public boolean deregisterListenerFromEventBus(String eventType, NFEventListener listener) {
		return listenerRegistry.deregisterListenerFromEventBus(eventType, listener);
	}

	/**
	 * Register NFEventListener to queue. You can only register one NFEventListener to each queue per application instance.
	 * If you want to further broker the NFEvent to other places, do it in the onEvent() method in the NFEventListener.
	 * You need to de-register a listener first if you want to register a new one on the same queueName, so taht the container is 
	 * stopped and destroyed.
	 * 
	 * @param queueName
	 * @param listener
	 * @return false if a listener already is registered on the same queueName.
	 */
	public boolean registerListenerToQueue(String queueName, final NFEventListener listener) {
		MessageListener msgListener = new MessageListener() {	
			public void onMessage(Message msg) {
				NFEvent event = null;
				try {
					ObjectMessage objMsg = (ObjectMessage) msg;
					event = (NFEvent) objMsg.getObject();
				} catch (Exception e) {
					logger.warn("Could not covert Message object to NFEvent object.\n" + e.toString());
				}
				
				if (event != null) {
					listener.onEvent(event);
				}
			}
		};
		
		DefaultMessageListenerContainer listenerContainer = createListenerContainer(queueName, msgListener, false);
		listenerContainer.start();
		
		return listenerRegistry.registerListenerToQueue(queueName, listenerContainer);
	}
	
	/**
	 * 
	 * @param queueName
	 * @param listener
	 * @return
	 */
	public boolean deregisterListenerFromQueue(String queueName) {
		return listenerRegistry.deregisterListenerFromQueue(queueName);
	}
	

	/**
	 * Broadcast event to the event bus.
	 * 
	 * @param eventType
	 * @param data
	 * @throws NFException 
	 */
	public void broadcastEvent(String eventType, Map<String, Object> data) throws NFException {
		NFEvent event = new NFEvent(eventType, data);
		try {
			broadcastEvent(event);
		} catch (Exception e) {
			throw new NFException(e.toString());
		}
	}

	/**
	 * Broadcast event to the event bus.
	 * 
	 * @param event
	 * @throws NFException 
	 */
	public void broadcastEvent(NFEvent event) throws NFException {
		if (connected) {
			ConnectionFactory connectionFactory = createConnectionFactory();
			TopicConnection conn;
			TopicSession session;
			Topic eventBusTopic;
			TopicPublisher publisher;
			try {
				conn = (TopicConnection) connectionFactory.createConnection();
				session = conn.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
				eventBusTopic = session.createTopic(eventBusTopicName);
				session.createPublisher(eventBusTopic);
				publisher = session.createPublisher(eventBusTopic);
				
				Message msg = session.createObjectMessage(event);
				publisher.send(msg);
				
				publisher.close();
				session.close();
				conn.close();
			} catch (JMSException e) {
				throw new NFException("Something went terrible wrong!\n" + e.toString());
			} 			
		} else {
			throw new NFException("NFService needs to be connected before you can broadcast a message to the event bus.");
		}
	}
	
	/**
	 * Send event to given queue.
	 * 
	 * @param queueName
	 * @param eventType
	 * @param data
	 * @throws NFException 
	 * @throws JMSException
	 */
	public void sendEvent(String queueName, String eventType, Map<String, Object> data) throws NFException {
		NFEvent event = new NFEvent(eventType, data);
		try {
			sendEvent(queueName, event);
		} catch (Exception e) {
			throw new NFException(e.toString());
		}
	}
	
	/**
	 * Send event to given queue.
	 * 
	 * @param queueName
	 * @param event
	 * @throws NFException
	 */
	public void sendEvent(String queueName, NFEvent event) throws NFException {
		if (brokerURL != null && brokerUsername != null && brokerPassword != null) {
			ConnectionFactory connectionFactory = createConnectionFactory();
			QueueConnection conn;
			QueueSession session;
			Queue queue;
			QueueSender sender;
			try {
				conn = (QueueConnection) connectionFactory.createConnection();
				session = conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
				queue = session.createQueue(queueName);
				sender = session.createSender(queue);
				
				Message msg = session.createObjectMessage(event);
				sender.send(msg);
				
				sender.close();
				session.close();
				conn.close();
			} catch (JMSException e) {
				throw new NFException(e.toString());
			}
		} else {
			throw new NFException("NFService needs to be configured with brokerURL, brokerUsername and "
					+ "brokerPassword before you can send a work item event to a queue.");
		}
	}
	
	/**
	 * Setter for event bus topic name.
	 * 
	 * @param eventBusName
	 */
	public void setEventBusName(String eventBusName) {
		this.eventBusTopicName = eventBusName;
	}
	
	/**
	 * Setter for the broker username.
	 * 
	 * @param brokerUsername
	 */
	public void setBrokerUserName(String brokerUsername) {
		this.brokerUsername = brokerUsername;
	}
	
	/**
	 * Setter for the broker password.
	 * 
	 * @param brokerPassword
	 */
	public void setBrokerPassword(String brokerPassword) {
		this.brokerPassword = brokerPassword;
	}
	
	/**
	 * Setter for the broker URL.
	 * 
	 * @param brokerURL
	 */
	public void setBrokerURL(String brokerURL) {
		this.brokerURL = brokerURL;
	}
	
}
