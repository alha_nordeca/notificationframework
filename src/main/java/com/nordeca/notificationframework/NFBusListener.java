package com.nordeca.notificationframework;

import java.util.List;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MessageListener that listens to the event bus topic and distributes the NFEvents to the registered NFEventListeners.
 * 
 * @author alha
 * 
 */
public class NFBusListener implements MessageListener {
	
	private static final Logger logger = LoggerFactory.getLogger(NFBusListener.class);
	private NFEventListenerRegistry listenerRegistry;
	
	public NFBusListener(NFEventListenerRegistry registry) {
		this.listenerRegistry = registry;
	}

	public void onMessage(Message msg) {
		NFEvent event = null;
		try {
			ObjectMessage objMsg = (ObjectMessage) msg;
			event = (NFEvent) objMsg.getObject();
		} catch (Exception e) {
			logger.warn("Could not covert Message object to NFEvent object.\n" + e.toString());
		}
		
		if (event != null) {
			List<NFEventListener> listeners = listenerRegistry.getEventBusListeners(event.getEventType());
			for (NFEventListener el : listeners) {
				el.onEvent(event);
			}
		}		
	}

}
