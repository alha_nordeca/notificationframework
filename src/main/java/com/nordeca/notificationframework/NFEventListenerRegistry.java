package com.nordeca.notificationframework;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.jms.listener.DefaultMessageListenerContainer;


/**
 * NFEvent listener registry class for registering/deregistering NFEventListeners on eventTypes on the event bus topic, 
 * and registering/deregistering NFEventListeners on queues with given queueName. 
 * 
 * @author alha
 *
 */
public class NFEventListenerRegistry {
	
	private Map<String, List<NFEventListener>> eventBusListeners;
	private Map<String, DefaultMessageListenerContainer> queueListeners;

	public NFEventListenerRegistry() {		
		eventBusListeners = new HashMap<String, List<NFEventListener>>();
		queueListeners = new HashMap<String, DefaultMessageListenerContainer>();
	}
	
	/**
	 * Register NFEventListener to listen to eventType on the event bus.
	 * 
	 * @param eventType
	 * @param listener
	 * @return true if listener if not already registered, false otherwise.
	 */
	public boolean registerListenerToEventBus(String eventType, NFEventListener listener) {
		List<NFEventListener> eventListeners = eventBusListeners.get(eventType);
		if (eventListeners == null) {
			eventListeners = new ArrayList<NFEventListener>();
			eventBusListeners.put(eventType, eventListeners);
		}
		
		return eventListeners.add(listener);
	}
	
	/**
	 * Deregister given NFEventListener for certain eventType.
	 * 
	 * @param eventType
	 * @param listener
	 * @return true if listener if already registered, false otherwise.
	 */
	public boolean deregisterListenerFromEventBus(String eventType, NFEventListener listener) {
		List<NFEventListener> eventListeners = eventBusListeners.get(eventType);
		boolean ret = eventListeners.remove(listener);
		
		// Keeping things tidy.
		if (eventListeners.isEmpty()) {
			eventBusListeners.remove(eventType);
		}
		
		return ret;
	}
	
	/**
	 * Register DefaultMessageListenerContainer to queue if there is not already registered a container with the queueName.
	 * 
	 * @param queueName
	 * @param listenerContainer
	 * @return false if there already is registered an listener.
	 */
	public boolean registerListenerToQueue(String queueName, DefaultMessageListenerContainer listenerContainer) {
		if (queueListeners.get(queueName) != null) {
			return false;
		}
		queueListeners.put(queueName, listenerContainer);
		
		return true;
	}
	
	/**
	 * Deregister DefaultMessageListenerContainer from queue.
	 * 
	 * @param queueName
	 * @param listenerContainer
	 * @return
	 */
	public boolean deregisterListenerFromQueue(String queueName) {
		DefaultMessageListenerContainer listenerContainer = queueListeners.get(queueName);
		if (listenerContainer == null) {
			return false;
		}
		listenerContainer.stop();
		listenerContainer.destroy();
		queueListeners.remove(queueName);
		return true;
	}
	
	/**
	 * Get the registered NFEventListeners for a given eventType.
	 * 
	 * @param eventType
	 * @return
	 */
	public List<NFEventListener> getEventBusListeners(String eventType) {
		return eventBusListeners.get(eventType);
	}
	
	/**
	 * Get the DefaultMessageListenerContainer for given queueName.
	 * 
	 * @param queueName
	 * @return
	 */
	public DefaultMessageListenerContainer getQueueListeningContainers(String queueName) {
		return queueListeners.get(queueName);
	}

}
