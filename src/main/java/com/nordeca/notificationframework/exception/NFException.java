package com.nordeca.notificationframework.exception;

public class NFException extends Exception {

	public NFException(String message) {
		super(message);
	}
}
