package com.nordeca.notificationframework;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * NFEvent pojo.
 * 
 * @author alha
 *
 */
@SuppressWarnings("serial")
public class NFEvent implements Serializable {

	private String eventType;
	private Map<String, Object> data;
	
	public NFEvent() {
		this.eventType = null;
		this.data = null;
	}
	
	public NFEvent(String eventType) {
		this.eventType = eventType;
		this.data = null;
	}
	
	public NFEvent(String eventType, Map<String, Object> data) {
		this.eventType = eventType;
		this.data = data;
	}
	
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public String getEventType() {
		return eventType;
	}
	
	public void setDataMap(Map<String, Object> data) {
		this.data = data;
	}
	
	public Map<String, Object> getDataMap() {
		return data;
	}
	
	public void putDataItem(String key, Object value) {
		if (data == null) {
			data = new HashMap<String, Object>();
		}
		data.put(key, value);
	}
	
	public Object getDataItem(String key) {
		return data.get(key);
	}
	
	public String toString() {
		String ret = "eventType: " + this.eventType + "\n";
		for (Map.Entry<String, Object> entry : this.data.entrySet()) {
			ret += entry.getKey() + ": " + entry.getValue().toString();
		}
		return ret;
	}
	
}
