package com.nordeca.notificationframework;

/**
 * NFEventListener interface for registering listeners to NFEvents on the event bus.
 * 
 * @author alha
 *
 */
public interface NFEventListener {
	
	void onEvent(NFEvent event);

}
